# Manual project

Project for manual

## For what?

This is bonus part of the second project in school 21.

## Task

### Bonus Quest 5*. Gitlab.

\> *Type in "Now that's not fair!"*

Let me remind you that no one said anything about being fair. It's my last request. You seem to be quite good at working with git. My library is missing some manuals, those for working with GitLab in particular. I don't like it when there is not enough information. Create a small and concise Markdown manual in src/gitlab_manual.md. I am concerned about the following issues, at least 5 of which should be backed up with screenshots:
1. creating a personal repository with the correct .gitignore and simple README.MD,
2. creating develop and master branches,
3. setting the develop branch as the default,
4. creating an issue for creating the current manual,
5. creating a branch for the issue,
6. creating a merge request on the develop branch,
7. commenting and accepting the request,
8. creating a stable version in the master with a tag,
9. working with wiki for the project.

Seems like everything is in place. If it's good enough, I'll upload it to the collection of the best manuals of the main library system and you'll be proud of yourself.

***== Quest 5 received. Prepare a brief manual on the points above on using GitLab in the src/gitlab_manual.md file, using Markdown. Attach a screenshot to at least 5 points. ==***

***LOADING...***

## Solution

You can find the solution in gitlab_manual.md